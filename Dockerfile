FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN xargs --null -L 1 pip install < requirements.txt

COPY . .

CMD [ "python", "./facedetect.py" ]