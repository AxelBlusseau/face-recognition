# Face detection module

## Specification

python 3+

## Installation

Create virtual env 
```bash
python -m venv ./venv
```

Activate virtual env
```bash
Windows:
venv/Scripts/activate

Linux/Mac:
source venv/bin/activate
```

Save packages inside requirements.txt
```bash
pip freeze > requirements.txt
```

Install dependencies

```bash
pip install -r requirements.txt
```

## Run project

```bash
py ./facedetect.py
```

## Run project using docker

Build it
```bash
docker build -t facedetect .
```

Run it
```bash
docker run -d facedetect --device=/dev/video0:/dev/video0 et croisez les doigts
```