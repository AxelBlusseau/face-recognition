import sys
import numpy as np
import cv2
import face_recognition
import psycopg2

############################
# DATABASE MANAGEMENT PART #
############################

INSERTION = "SELECT public.add_datas(%s, %s, %s)"

def insertDatas(idTrajet, nbUser, motif):
    try:
        connection = psycopg2.connect(user="postgres", password="admin", host="127.0.0.1", port="5432", database="workshop")
        cursor = connection.cursor()
        cursor.execute(INSERTION, [idTrajet, nbUser, motif])
        connection.commit()

    except (Exception, psycopg2.Error) as error:
        if(connection):
            print("Failed to insert record", error)

    finally:
        # closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")


#########################
# FACE RECOGNITION PART #
#########################

# Args Check
if len(sys.argv) <= 1:
    print("Veuillez renseigner l'id du trajet")
    exit()

video_capture = cv2.VideoCapture(0)

# Create arrays of known face encodings and their names
known_face_encodings = []

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True
uniq_person = 0

while True:
    # Grab a single frame of video
    ret, frame = video_capture.read()

    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # See if the face is a match for the known face(s)
            name = "Unknown"
            if len(known_face_encodings) > 0:
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
                # # If a match was found in known_face_encodings, just use the first one.
                if True in matches:
                    name = "Known"
                    
            face_names.append(name)
            known_face_encodings.append(face_encoding)
            if name == "Unknown":
                uniq_person += 1
    process_this_frame = not process_this_frame


    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # Draw a label with a name below the face
        cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        #Print passangers number
        cv2.putText(frame, "Passengers : " + str(uniq_person), (0,30), font, 1.0, (255, 255, 255), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        #SQL REQUEST
        comment = ""
        if len(sys.argv) == 3:
            comment = sys.argv[2]
        insertDatas(sys.argv[1], uniq_person, comment)
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()